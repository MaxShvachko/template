import React from "react"
var sinon = require('sinon');
import Enzyme from 'enzyme';
import {assert, expect } from 'chai';
import {Layout} from "../../../modules/Layout.jsx";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import mochaSnapshots from 'mocha-snapshots';
configure({ adapter: new Adapter() });
mochaSnapshots.setup({ sanitizeClassNames: false });

import {JSDOM} from 'jsdom';

const dom = new JSDOM(`<!DOCTYPE html><html lang="en"><head></head><body></body></html>`);
global.window = dom.window;
global.document = dom.window.document;

describe('<Layout />', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(<Layout />);
        // You can match Enzyme wrappers
        expect(wrapper).to.matchSnapshot();
        // Strings
        expect('you can match strings').to.matchSnapshot();
        // Numbers
        expect(123).to.matchSnapshot();
        // Or any object
        expect({ a: 1, b: { c: 1 } }).to.matchSnapshot();
    });

    it('mount ', () => {
        //Given
        const layout = mount (<Layout />);
        sinon.spy(layout.changeState);

        layout.find("btn").simulate("click");

        sinon.assert.calledWidth(layout.changeState, false);
        assert.isTrue(layout.enter);

        console.log(layout);
    });
});